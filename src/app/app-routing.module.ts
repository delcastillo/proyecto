import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './pages/contact/contact.component';
import { CrudComponent } from './pages/crud/crud.component';
import { homeComponent } from './pages/home/home.component';
import { LocationComponent } from './pages/location/location.component';
import { MembersComponent } from './pages/members/members.component';
import { TeamComponent } from './pages/team/team.component';

const routes: Routes = [
  {path:'Inicio', component: homeComponent},
  {path:'Habitaciones', component: TeamComponent},
  {path:'Contacto', component: ContactComponent},
  {path:'Reservas', component: LocationComponent},
  {path:'members', component: MembersComponent},
  {path:'CRUD', component: CrudComponent},
  {path:'**',pathMatch:'full',redirectTo:'Inicio'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
