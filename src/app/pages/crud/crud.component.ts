import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ISettlementCalculation } from './isettlement-calculation';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.scss']
})
export class CrudComponent implements OnInit {

  public FormCRUD: FormGroup;
  public CurrentIndex: number;
  public Reservations: Array<ISettlementCalculation>;

  constructor() {
    let reservations = localStorage.getItem('Reservations');
    this.Reservations = reservations ? JSON.parse(reservations) : [];
    // this.Current = <ISettlementCalculation>{};
    this.CurrentIndex = -1;
    this.FormCRUD = new FormGroup({
      FirstName: new FormControl('', Validators.required),
      LastName:  new FormControl('', Validators.required),
      NightValue:  new FormControl(0, Validators.min(1)),
      NightsCount:  new FormControl(0, Validators.min(1))
    });
  }

  ngOnInit(): void {
  }

  SubmitCRUD(): void {
    let c = <ISettlementCalculation>{...this.FormCRUD.value};
    c.Total = c.NightValue * c.NightsCount;
    if (this.CurrentIndex >= 0 && this.CurrentIndex < this.Reservations.length) {
      this.Reservations[this.CurrentIndex] = c;
    } else {
      this.Reservations.push(c);
    }
    this.ResetCRUD();
    this.StoreOnLocalStorage();
  }

  EditElementAtIndex(index: number): void {
    if (index < this.Reservations.length) {
      this.CurrentIndex = index;
      let c = this.Reservations[index];
      this.FormCRUD.get('FirstName')?.patchValue(c.FirstName);
      this.FormCRUD.get('LastName')?.patchValue(c.LastName);
      this.FormCRUD.get('NightValue')?.patchValue(c.NightValue);
      this.FormCRUD.get('NightsCount')?.patchValue(c.NightsCount);
    }
  }
  
  RemoveElementAtIndex(index: number): void {
    if (index < this.Reservations.length) {
      this.Reservations.splice(index, 1);
      this.StoreOnLocalStorage();
    }
  }

  ResetCRUD(): void {
    this.CurrentIndex = -1;
    this.FormCRUD.reset();
  }

  StoreOnLocalStorage(): void {
    localStorage.setItem('Reservations', JSON.stringify(this.Reservations));
  }

}
