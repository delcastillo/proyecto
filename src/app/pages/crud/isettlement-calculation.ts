export interface ISettlementCalculation {
    FirstName: string;
    LastName: string;
    NightValue: number;
    NightsCount: number;
    Total: number;
}
