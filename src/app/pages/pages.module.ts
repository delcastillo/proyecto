import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { homeComponent } from './home/home.component';
import { TeamComponent } from './team/team.component';
import { ContactComponent } from './contact/contact.component';
import { LocationComponent } from './location/location.component';
import { MembersComponent } from './members/members.component';
import { CrudComponent } from './crud/crud.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    homeComponent,
    TeamComponent,
    ContactComponent,
    LocationComponent,
    MembersComponent,
    CrudComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PagesModule { }
